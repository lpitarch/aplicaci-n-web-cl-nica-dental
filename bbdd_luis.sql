-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-03-2014 a las 16:16:42
-- Versión del servidor: 5.5.35
-- Versión de PHP: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bbdd_luis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Albaran`
--

CREATE TABLE IF NOT EXISTS `Albaran` (
  `id_albaran` int(4) NOT NULL,
  `Fecha_emision_albaran` varchar(45) DEFAULT NULL,
  `Factura_id_factura` int(4) NOT NULL,
  `Detalle_albaran_id_detalle` int(4) NOT NULL,
  PRIMARY KEY (`id_albaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE IF NOT EXISTS `articulos` (
  `id_articulo` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `concepto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_articulo`),
  UNIQUE KEY `id_articulo_UNIQUE` (`id_articulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id_articulo`, `concepto`) VALUES
(1, 'ADAMS ( ORTODONCIA)'),
(2, 'ANALOGO'),
(3, 'ANALOGO KLORNER'),
(4, 'AÑADIR 1ª PIEZA '),
(5, 'AÑADIR RESTO PIEZAS'),
(6, 'ARANDELA DE ORO'),
(7, 'ARCO VESTIBULAR ( ORTODONCIA)'),
(8, 'ATACHE PIEZA FIJA'),
(9, 'BARRA ACKERMAN'),
(10, 'CALCINABLE'),
(11, 'CALCINABLE KLORNER'),
(12, 'CAMBIO DE COLOR '),
(13, 'CAMBIO TOTAL RESINA HIBRIDAS'),
(14, 'CAMBIO TOTAL RESINA HIBRIDAS C/ SOLDADURAS'),
(15, 'CARILLA EMPRES'),
(16, 'COMPLETA '),
(17, 'COMPLETA DEFINITIVA'),
(18, 'COMPLETA PROVISIONAL '),
(19, 'COMPOSTURA'),
(20, 'COMPOSTURA// REBASE'),
(21, 'CONTENEDOR ESPACIO'),
(22, 'CORONA CIRCONIO'),
(23, 'CORONA CIRCONIO SOBRE IMPLANTE'),
(24, 'CORONA EMPRESS SIN METAL'),
(25, 'CORONA JACKET CERAMICA'),
(26, 'CORONA METAL CERAMICA'),
(27, 'CORONA PROVISIONAL CEMENTADA'),
(28, 'CORONA SOBRE IMPLANTE'),
(29, 'ESQUELETICO BILATERAL 1-3 PZAS'),
(30, 'ESQUELETICO BILATERAL 3-5 PZAS'),
(31, 'ESQUELETICO BILATERAL 5-8 PZAS'),
(32, 'ESQUELETICO BILATERAL 8-14 PZAS'),
(33, 'ESQUELETICO UNILATERAL 1-3 PZAS'),
(34, 'FERULA DE BLANQUEAMIENTO'),
(35, 'FERULA DESCARGA'),
(36, 'FERULA QUIRURGICA'),
(37, 'GANCHO UNIDAD'),
(38, 'IMPLANTE PROVISIONAL'),
(39, 'IMPLANTE PROVISIONAL ATORNILLADO'),
(40, 'INTERFASE CIRCONIO'),
(41, 'LOCATOR UNIDAD'),
(42, 'MERYLAND PIEZA'),
(43, 'PARCIAL ACRILICO 10 PZAS O MÁS'),
(44, 'PARCIAL ACRILICO DE 1-3 PZAS'),
(45, 'PARCIAL ACRILICO DE 4 PZAS'),
(46, 'PARCIAL ACRILICO DE 5 PZAS'),
(47, 'PARCIAL ACRILICO DE 6 PZAS'),
(48, 'PARCIAL ACRILICO DE 7 PZAS'),
(49, 'PARCIAL ACRILICO DE 8 PZAS'),
(50, 'PARCIAL ACRILICO DE 9 PZAS'),
(51, 'PARCIAL ACRILICO PROV. 10 PZAS O MÁS'),
(52, 'PARCIAL ACRILICO PROV. DE 1-3 PZAS'),
(53, 'PARCIAL ACRILICO PROV. DE 4 PZAS'),
(54, 'PARCIAL ACRILICO PROV. DE 5 PZAS'),
(55, 'PARCIAL ACRILICO PROV. DE 6 PZAS'),
(56, 'PARCIAL ACRILICO PROV. DE 7 PZAS'),
(57, 'PARCIAL ACRILICO PROV. DE 8 PZAS'),
(58, 'PARCIAL ACRILICO PROV. DE 9 PZAS'),
(59, 'PERNO MUÑON COLADO'),
(60, 'PERNO MUÑON DOBLE COLADO'),
(61, 'PILAR ANGULADO'),
(62, 'PILAR CIRCONIO'),
(63, 'PILAR KLORNER ANGULADO'),
(64, 'PILAR KLORNER RECTO'),
(65, 'PILAR RECTO'),
(66, 'PLACA ( ORTODONCIA)'),
(67, 'PROTESIS HIBRIDA'),
(68, 'REPARACION ARCADA CERAMICA'),
(69, 'REPARACION SOBREDENTADURA C/ REFUERZO'),
(70, 'REPARACION SOBREDENTADURA S/ REFUERZO'),
(71, 'SET UP UNIDAD'),
(72, 'SOBREDENTADURA CON LOCATOR'),
(73, 'SOLDADURA'),
(74, 'TORNILLO AVINENT'),
(75, 'TORNILLO EXPANSION( ORTODONCIA)'),
(76, 'TORNILLO KLORNER'),
(77, 'TORNILLO TITANIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_materiales`
--

CREATE TABLE IF NOT EXISTS `articulos_materiales` (
  `materiales_id_material` int(4) unsigned NOT NULL,
  `articulos_id_articulo` int(4) unsigned NOT NULL,
  PRIMARY KEY (`materiales_id_material`,`articulos_id_articulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulos_materiales`
--

INSERT INTO `articulos_materiales` (`materiales_id_material`, `articulos_id_articulo`) VALUES
(1, 13),
(1, 14),
(1, 17),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 67),
(2, 9),
(2, 14),
(2, 21),
(2, 26),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 37),
(2, 59),
(2, 60),
(2, 67),
(2, 70),
(2, 72),
(2, 73),
(3, 22),
(3, 23),
(3, 40),
(3, 62),
(4, 18),
(4, 51),
(4, 52),
(4, 53),
(4, 54),
(4, 55),
(4, 56),
(4, 57),
(4, 58),
(5, 67),
(6, 4),
(6, 5),
(6, 12),
(6, 17),
(6, 29),
(6, 30),
(6, 31),
(6, 32),
(6, 33),
(6, 43),
(6, 44),
(6, 45),
(6, 46),
(6, 47),
(6, 48),
(6, 49),
(6, 50),
(6, 72),
(7, 67),
(8, 15),
(8, 23),
(8, 24),
(8, 25),
(9, 22),
(9, 26),
(9, 28),
(9, 68),
(10, 22),
(10, 26),
(10, 28),
(11, 35),
(11, 66),
(12, 34),
(12, 36),
(13, 72),
(14, 72),
(16, 19),
(17, 18),
(17, 51),
(17, 52),
(17, 53),
(17, 54),
(17, 55),
(17, 56),
(17, 57),
(17, 58),
(17, 69),
(17, 70),
(18, 27),
(18, 39),
(18, 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinicas`
--

CREATE TABLE IF NOT EXISTS `clinicas` (
  `id_clinica` int(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `razonsocial` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cif` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provincia` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numclinica` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarifas_id_tarifa` int(4) unsigned NOT NULL,
  PRIMARY KEY (`id_clinica`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `clinicas`
--

INSERT INTO `clinicas` (`id_clinica`, `nombre`, `razonsocial`, `cif`, `localidad`, `provincia`, `direccion`, `cp`, `numclinica`, `tarifas_id_tarifa`) VALUES
(1, 'CENTRO DE IMPLANTOLOGÍA TARRAGONA', 'XX', 'B11111117', 'MADRID', 'TARRAGONA', 'Paseo de Tarragona nº 21', '28020', '15', 1),
(3, 'CENTRO DE IMPLANTOLOGÍA VIGO', 'XX', 'B11111113', 'VIGO', 'PONTEVEDRA', 'Calle de Vigo nº 17', '36208', '3', 1),
(4, 'CENTRO DE IMPLANTOLOGÍA ZARAGOZA', 'XX', 'B11111114', 'ZARAGOZA', 'ZARAGOZA', 'Paseo Independencia nº 18', '50005', '4', 1),
(5, 'CENTRO DE IMPLANTOLOGÍA VALENCIA', 'XX', 'B11111115', 'VALENCIA', 'VALENCIA', 'Calle de Valencia nº 19', '46018', '5', 1),
(6, 'CENTRO DE IMPLANTOLOGÍA SANTANDER', 'XX', 'B11111116', 'SANTANDER', 'SANTANDER', 'Calle de Santander nº 20', '50008', '6', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinica_doctor`
--

CREATE TABLE IF NOT EXISTS `clinica_doctor` (
  `id_doctor` int(4) NOT NULL AUTO_INCREMENT,
  `id_clinica` int(4) NOT NULL,
  `numdoctor` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_doctor`,`id_clinica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `clinica_doctor`
--

INSERT INTO `clinica_doctor` (`id_doctor`, `id_clinica`, `numdoctor`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 6, 2),
(4, 1, 4),
(4, 2, 1),
(5, 2, 4),
(6, 2, 2),
(7, 4, 1),
(8, 2, 3),
(9, 3, 2),
(10, 3, 1),
(11, 1, 2),
(11, 4, 2),
(11, 6, 1),
(12, 5, 1),
(13, 6, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_albaran`
--

CREATE TABLE IF NOT EXISTS `Detalle_albaran` (
  `id_detalle` int(4) NOT NULL,
  `Tarifa` decimal(10,2) DEFAULT NULL,
  `Articulo` varchar(45) DEFAULT NULL,
  `Precio_unidad` decimal(10,2) DEFAULT NULL,
  `Cantidad` decimal(10,2) DEFAULT NULL,
  `Precio_total` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_material`
--

CREATE TABLE IF NOT EXISTS `Detalle_material` (
  `id_material` int(4) NOT NULL,
  `Concepto` varchar(45) DEFAULT NULL,
  `Detalle_albaran_id_detalle` int(4) NOT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctores`
--

CREATE TABLE IF NOT EXISTS `doctores` (
  `id_doctor` int(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `numcolegiado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_doctor`),
  UNIQUE KEY `id_doctor_UNIQUE` (`id_doctor`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `doctores`
--

INSERT INTO `doctores` (`id_doctor`, `nombre`, `numcolegiado`) VALUES
(1, 'DAVID PEREZ', '11111'),
(2, 'FRANCISCO JIMENEZ', '22222'),
(3, 'ESTEBAN GONZALEZ', '333333'),
(4, 'DAVINIA DE DOS SANTOS', '44444'),
(5, 'JORGE ESPIAS', '55555'),
(6, 'LORENZO CAMAÑÓN', '666666'),
(7, 'HUGO IDIAZABAL', '777777'),
(8, 'JACOBO MADORRÁN', '888888'),
(9, 'MANUEL LOPEZ', '999999'),
(10, 'ADRIANA MEAÑOS', '101010'),
(11, 'LAURA SERRANO', '12121212'),
(12, 'YASMINE JERARCAL', '13131313'),
(13, 'DIEGO ZAERA', '14141414');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Factura`
--

CREATE TABLE IF NOT EXISTS `Factura` (
  `id_factura` int(4) NOT NULL,
  `fecha_emision_factura` varchar(45) DEFAULT NULL,
  `Importe total` decimal(10,2) DEFAULT NULL,
  `Clinica` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Historial`
--

CREATE TABLE IF NOT EXISTS `Historial` (
  `id_historial` int(4) NOT NULL AUTO_INCREMENT,
  `Fecha_solicitud` date DEFAULT NULL,
  `Fecha_entrada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL,
  `Estado` varchar(20) DEFAULT NULL,
  `Fecha_Nueva_cita` date DEFAULT NULL,
  `Prescripcion_id_prescripcion` int(4) NOT NULL,
  `Albaran_id_albaran` int(4) NOT NULL,
  PRIMARY KEY (`id_historial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='tipo_incidencias_id_incidencia' AUTO_INCREMENT=47 ;

--
-- Volcado de datos para la tabla `Historial`
--

INSERT INTO `Historial` (`id_historial`, `Fecha_solicitud`, `Fecha_entrada`, `Fecha_salida`, `Estado`, `Fecha_Nueva_cita`, `Prescripcion_id_prescripcion`, `Albaran_id_albaran`) VALUES
(43, '2014-03-07', '2014-03-21', '2014-03-19', 'aceptado', '2014-03-12', 7, 0),
(44, '2014-03-09', '2014-03-15', '2014-03-29', 'en proceso', '2014-03-08', 5, 0),
(45, '2014-03-09', '2014-03-22', '2014-03-28', 'probando', '0000-00-00', 9, 0),
(46, '2014-03-16', '2014-03-23', '2014-03-20', 'recibido', '2014-03-30', 6, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiales`
--

CREATE TABLE IF NOT EXISTS `materiales` (
  `id_material` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lote1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lote2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_material`),
  UNIQUE KEY `id_tarifa` (`id_material`),
  UNIQUE KEY `concepto` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `materiales`
--

INSERT INTO `materiales` (`id_material`, `nombre`, `lote1`, `lote2`) VALUES
(1, 'PALAPRESS VARIO 500ML LIQ/ POLVO', NULL, NULL),
(2, 'C+B CR-CO 1KG', NULL, NULL),
(3, 'CIRCONIO', NULL, NULL),
(4, 'DIENTES INTEGRAL ', NULL, NULL),
(5, 'DIENTES MONDIAL ', NULL, NULL),
(6, 'DIENTES MONDIAL / PREMIUM', NULL, NULL),
(7, 'DIENTES PREMIUM', NULL, NULL),
(8, 'EMAX CERAMICA', NULL, NULL),
(9, 'IPS CERAMICA INLINE DENTIN', NULL, NULL),
(10, 'OPAQUER', NULL, NULL),
(11, 'ORTHORESIN ROSA LIQ/ POLVO', NULL, NULL),
(12, 'ORTHORESIN TRANS LIQ/ POLVO', NULL, NULL),
(13, 'PALAPRESS VARIO 1KG ROSA VETEADO POLVO', NULL, NULL),
(14, 'PALAPRESS VARIO 500ML LIQ', NULL, NULL),
(16, 'PALAPRESS VARIO LIQ/ POLVO', NULL, NULL),
(17, 'RAPID REPAIR LIQUIDO/ POLVO', NULL, NULL),
(18, 'TRIAD PROVIS. MARFIL MEDIO', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Prescripcion`
--

CREATE TABLE IF NOT EXISTS `Prescripcion` (
  `id_prescripcion` int(4) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Numero_paciente` int(4) DEFAULT NULL,
  `Nombre_paciente` varchar(45) DEFAULT NULL,
  `clinica_doctor_id_doctor` int(4) NOT NULL,
  `clinica_doctor_id_clinica` int(4) NOT NULL,
  PRIMARY KEY (`id_prescripcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `Prescripcion`
--

INSERT INTO `Prescripcion` (`id_prescripcion`, `Descripcion`, `Numero_paciente`, `Nombre_paciente`, `clinica_doctor_id_doctor`, `clinica_doctor_id_clinica`) VALUES
(5, 'empaste', 88888, 'Andres lopez', 1, 1),
(6, 'puente', 99999, 'Luis', 4, 1),
(7, 'ortodoncia', 77777, 'Antonio Garcia', 3, 4),
(9, 'muela picada', 333333, 'Paco Perez', 10, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifas`
--

CREATE TABLE IF NOT EXISTS `tarifas` (
  `id_tarifa` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tarifa`),
  UNIQUE KEY `id_tarifa` (`id_tarifa`),
  UNIQUE KEY `concepto` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tarifas`
--

INSERT INTO `tarifas` (`id_tarifa`, `nombre`, `descripcion`) VALUES
(1, 'TARIFA 1', NULL),
(2, 'TARIFA 2', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifas_articulos`
--

CREATE TABLE IF NOT EXISTS `tarifas_articulos` (
  `tarifa` decimal(10,2) DEFAULT NULL,
  `tarifas_id_tarifa` int(4) unsigned NOT NULL,
  `articulos_id_articulo` int(4) unsigned NOT NULL,
  PRIMARY KEY (`articulos_id_articulo`,`tarifas_id_tarifa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tarifas_articulos`
--

INSERT INTO `tarifas_articulos` (`tarifa`, `tarifas_id_tarifa`, `articulos_id_articulo`) VALUES
(13.00, 1, 1),
(4.00, 1, 2),
(4.00, 2, 2),
(8.00, 1, 3),
(8.00, 2, 3),
(20.00, 1, 4),
(35.00, 2, 4),
(10.00, 1, 5),
(15.00, 2, 5),
(17.00, 1, 6),
(17.00, 2, 6),
(18.00, 1, 7),
(25.00, 1, 8),
(50.00, 2, 8),
(330.00, 1, 9),
(400.00, 2, 9),
(4.00, 1, 10),
(4.00, 2, 10),
(8.00, 1, 11),
(8.00, 2, 11),
(15.00, 1, 12),
(15.00, 2, 12),
(300.00, 1, 13),
(340.00, 1, 14),
(90.00, 1, 15),
(100.00, 2, 15),
(110.00, 2, 16),
(90.00, 1, 17),
(90.00, 1, 18),
(110.00, 2, 18),
(20.00, 1, 19),
(30.00, 2, 20),
(50.00, 1, 21),
(60.00, 2, 21),
(100.00, 1, 22),
(120.00, 2, 22),
(130.00, 1, 23),
(150.00, 2, 23),
(85.00, 1, 24),
(100.00, 2, 24),
(90.00, 1, 25),
(50.00, 1, 26),
(65.00, 2, 26),
(12.00, 1, 27),
(18.00, 2, 27),
(60.00, 1, 28),
(120.00, 2, 28),
(96.00, 1, 29),
(130.00, 2, 29),
(140.00, 1, 30),
(160.00, 2, 30),
(160.00, 1, 31),
(180.00, 2, 31),
(180.00, 1, 32),
(210.00, 2, 32),
(80.00, 1, 33),
(110.00, 2, 33),
(30.00, 1, 34),
(30.00, 2, 34),
(60.00, 1, 35),
(70.00, 2, 35),
(30.00, 1, 36),
(30.00, 2, 36),
(10.00, 1, 37),
(10.00, 2, 37),
(25.00, 2, 38),
(22.00, 1, 39),
(35.00, 1, 40),
(35.00, 2, 40),
(110.00, 1, 41),
(110.00, 2, 41),
(22.00, 1, 42),
(25.00, 2, 42),
(90.00, 1, 43),
(110.00, 2, 43),
(35.00, 1, 44),
(45.00, 2, 44),
(40.00, 1, 45),
(50.00, 2, 45),
(50.00, 1, 46),
(60.00, 2, 46),
(62.00, 1, 47),
(72.00, 2, 47),
(80.00, 1, 48),
(90.00, 2, 48),
(88.00, 1, 49),
(98.00, 2, 49),
(90.00, 1, 50),
(100.00, 2, 50),
(90.00, 1, 51),
(110.00, 2, 51),
(35.00, 1, 52),
(45.00, 2, 52),
(40.00, 1, 53),
(50.00, 2, 53),
(50.00, 1, 54),
(60.00, 2, 54),
(62.00, 1, 55),
(72.00, 2, 55),
(80.00, 1, 56),
(90.00, 2, 56),
(88.00, 1, 57),
(98.00, 2, 57),
(90.00, 1, 58),
(100.00, 2, 58),
(29.00, 1, 59),
(29.00, 2, 59),
(49.00, 1, 60),
(49.00, 2, 60),
(25.00, 1, 61),
(25.00, 2, 61),
(125.00, 1, 62),
(125.00, 2, 62),
(8.00, 1, 63),
(8.00, 2, 63),
(8.00, 1, 64),
(8.00, 2, 64),
(20.00, 1, 65),
(25.00, 2, 65),
(60.00, 1, 66),
(395.00, 1, 67),
(600.00, 2, 67),
(360.00, 1, 68),
(390.00, 2, 68),
(35.00, 1, 69),
(40.00, 2, 69),
(20.00, 1, 70),
(30.00, 2, 70),
(10.00, 1, 71),
(10.00, 2, 71),
(180.00, 1, 72),
(240.00, 2, 72),
(30.00, 1, 73),
(30.00, 2, 73),
(14.00, 1, 74),
(14.00, 2, 74),
(5.00, 1, 75),
(8.00, 1, 76),
(8.00, 2, 76),
(4.00, 1, 77),
(4.00, 2, 77);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_incidencias`
--

CREATE TABLE IF NOT EXISTS `tipo_incidencias` (
  `id_incidencia` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `Descripcion` varchar(300) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `Historial_id_historial` int(4) NOT NULL,
  PRIMARY KEY (`id_incidencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `tipo_incidencias`
--

INSERT INTO `tipo_incidencias` (`id_incidencia`, `tipo`, `Descripcion`, `Historial_id_historial`) VALUES
(1, 'grave', 'puente roto', 22),
(12, 'asdfasrer', 'mufdfdfd', 21),
(13, 'medio22', 'muelle doblado', 21),
(15, 'dfd', 'dfdf', 31),
(18, 'ztyty22', 'tyyty22', 29),
(19, 'sdf', 'sdf', 37),
(20, 'sdfasdf', 'sdfasdf', 29),
(22, 'grave', 'descripcion asdf', 44),
(23, 'medio', 'xcvxcv bs', 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `usuario` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_clinica` int(4) DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
