<?php

/* Database connection information */
include("mysql.php" );

/*
 * Local functions
 */

function fatal_error($sErrorMessage = '') {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
    die($sErrorMessage);
}

/*
 * MySQL connection
 */
if (!$gaSql['link'] = mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password'])) {
    fatal_error('Could not open connection to server');
}

if (!mysql_select_db($gaSql['db'], $gaSql['link'])) {
    fatal_error('Could not select database ');
}

mysql_query('SET names utf8');



/*
 * SQL queries
 * Get data to display
 */

$sQuery = "SELECT  doctores.id_doctor , doctores.nombre 
	    
	    FROM clinicas
	    	INNER JOIN clinica_doctor   
	            ON clinicas.id_clinica = clinica_doctor.id_clinica
			INNER JOIN doctores 
				ON doctores.id_doctor = clinica_doctor.id_doctor

where clinicas.id_clinica = ".$_REQUEST['id_clinica'];
//$sQuery = "select id_doctor, nombre from doctores order by nombre";

$rResult = mysql_query($sQuery, $gaSql['link']) or fatal_error('MySQL Error: ' . mysql_errno());

$resultado = array();
while ($fila = mysql_fetch_array($rResult)) {
    $resultado[] = array(
      'id_doctor' => $fila['id_doctor'],
      'nombre' => $fila['nombre']
   );
}
echo json_encode($resultado);
?>
